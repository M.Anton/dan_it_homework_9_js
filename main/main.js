function showListFromArray(array, parent = document.body) {

    let ul = document.createElement('ul');
    parent.prepend(ul);

    for (let arrayElement of array) {
        let li = document.createElement('li');
        ul.append(li);

        if ( Array.isArray(arrayElement) ) {
            showListFromArray(arrayElement, li);
        }

        else {
            li.textContent = arrayElement;
        }
    }
}

let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];;

showListFromArray(arr3);


function clearAll() {
    document.body.innerHTML = "";
}

function timer() {
    let timeLeft = 3;
    let sec = document.createElement('h1');
    document.body.prepend(sec);
    sec.style.textAlign = "center";
    sec.textContent = timeLeft;
    const timerInterval = setInterval(() => {
    timeLeft--;
    sec.textContent = timeLeft;
    if (timeLeft === -1) {
    clearInterval(timerInterval);
    clearAll();
    }

    }, 1000);
}

timer();